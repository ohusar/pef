import unittest

from .test_runner import TestRunner


class Runner:
    def __init__(self, test_case, test_runner=TestRunner):
        loader = unittest.TestLoader()
        suite = loader.loadTestsFromTestCase(test_case)
        runner = test_runner()

        result = runner.run(suite)
        result.check_prerequisites(runner)
        result.print_outcome()

        self.points = result.points
