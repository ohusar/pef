import unittest
from io import StringIO
import pycodestyle

from .lib.import_analyser import ImportAnalyser
from .lib.keyword_analyser import KeywordAnalyser
from .lib.class_structure_checker import ClassStructureChecker
from .result import TestResult


class TestRunner(unittest.TextTestRunner):
    resultclass = TestResult

    def __init__(self, *args, **kwargs):
        kwargs['stream'] = StringIO()
        super().__init__(*args, **kwargs)
        self.set_defaults()
        self.configure()
        if not self.testing:
            self.assertImportsUsed()
            self.assertClassStructure()
            self.assertKeywordsUsed()
            self.assertPEPConformance()

    def set_defaults(self):
        self.testing = False
        self.module_name = 'riesenie'

        self.has_disallowed_imports = False
        self.is_missing_class = False
        self.has_disallowed_keywords = False
        self.has_pep_violations = False

        self.imports_msg = ''
        self.class_msg = ''
        self.keywords_msg = ''
        self.pep_msg = ''

        self.allowed_imported_modules = set()
        self.required_class_structure = dict()
        self.banned_keywords = set()
        self.allowed_keywords = {
            'and', 'del', 'from', 'not', 'while', 'as', 'elif', 'global', 'or', 'with',
            'assert', 'else', 'if', 'pass', 'yield', 'break', 'except', 'import', 'print',
            'class', 'exec', 'in', 'raise', 'continue', 'finally', 'is', 'return', 'def',
            'for', 'lambda', 'try', 'True', 'False', 'None'
        }
        self.check_pep_rules = True

    def configure(self):
        pass

    def assertImportsUsed(self):
        if isinstance(self.allowed_imported_modules, str) and \
           self.allowed_imported_modules == 'all':
            return

        used_imports = ImportAnalyser.get_names_in_scope(self.module_name + '.py')

        failure_msg = 'You can not use these imports: {} -' \
                      ' only allowed imports are {}.'
        used_disallowed_imports = used_imports - self.allowed_imported_modules

        self.has_disallowed_imports = len(used_disallowed_imports) > 0
        self.imports_msg = failure_msg.format(
            used_disallowed_imports, self.allowed_imported_modules
        ) if self.has_disallowed_imports else ''

    def assertClassStructure(self):
        if not isinstance(self.required_class_structure, list):
            self.required_class_structure = [self.required_class_structure]

        for class_structure in self.required_class_structure:
            has_required_classes = ClassStructureChecker.check_class_structure(
                self.module_name,
                class_structure
            )
            self.is_missing_class = self.is_missing_class or not has_required_classes
            self.class_msg = 'Module does not have required classes defined. Expected: {}'.format(
                class_structure
            ) if self.is_missing_class else self.class_msg

    def assertKeywordsUsed(self):
        keywords = KeywordAnalyser.get_keywords_used(self.module_name + '.py')
        failure_msg = 'These keywords are not allowed to be used: {}.'
        if self.banned_keywords:
            equal_keywords = self.banned_keywords.intersection(keywords)
            failure_msg = failure_msg.format(equal_keywords)
            has_disallowed_keywords = len(equal_keywords) > 0
        else:
            keywords_without_allowed = keywords - self.allowed_keywords
            failure_msg = failure_msg.format(keywords_without_allowed)
            has_disallowed_keywords = len(keywords_without_allowed) > 0

        self.has_disallowed_keywords = has_disallowed_keywords
        self.keywords_msg = failure_msg if has_disallowed_keywords else ''

    def assertPEPConformance(self):
        """Test that module conforms to PEP-8."""
        style = pycodestyle.StyleGuide(quiet=True, config_file='tox.ini')
        result = style.check_files([self.module_name + '.py'])
        self.has_pep_violations = result.total_errors > 0
        msg = "Found code style errors (and warnings). Namely:"
        
        for report in result.get_statistics():
            msg += '\n{}'.format(report)
        self.pep_msg = msg if self.has_pep_violations else 'CODE STYLE CHECK PERFECT!'
