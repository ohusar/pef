"""
Module for displaying trees in ASCII.

Greatly inspired by Stu Gla's answer to stack overflow question:
http://stackoverflow.com/questions/15675261/displaying-a-tree-in-ascii

Answered on Jul 4 '13 at 5:32.
"""

import itertools

from .attribute_finder import AttributeFinder
from .custom_exceptions import AttributeNotFound
from .default_structure_attributes import *


class TreeVisualisation:
    @staticmethod
    def ascii_representation(node, max_depth=None):
        """
        Computes ASCII representation for tree node. Tree node should have children attribute/
        method.

        NOTE: Works best with consistent node name lengths.

        :param max_depth: max depth till which to display the tree
        :param node: tree node
        :return: string containing ASCII representation of a tree
        """
        if node is None:
            return '<class \'NoneType\'>'

        return TreeVisualisation._ascii_representation(node, 0, max_depth)

    @staticmethod
    def _ascii_representation(node, depth, max_depth=None):

        if max_depth is not None and depth == max_depth:
            return ''

        name, children = TreeVisualisation._get_expected_node_attributes(node)

        if not children:
            return name

        child_strings = [
            TreeVisualisation._ascii_representation(child, depth + 1, max_depth)
            for child in children
        ]
        child_widths = [TreeVisualisation.block_width(s) for s in child_strings]

        display_width = TreeVisualisation.get_block_width(name, child_widths)
        child_midpoints = TreeVisualisation.determine_midpoints(child_widths)
        brace = TreeVisualisation.build_brace(display_width, child_midpoints)

        formatted_name = '{:^{}}'.format(name, display_width)

        nodes_bellow = TreeVisualisation.stack_str_blocks(child_strings)

        return formatted_name + '\n' + brace + '\n' + nodes_bellow

    @staticmethod
    def _get_expected_node_attributes(node, attrs=DEFAULT_NODE_ATTRS):
        try:
            node_attributes = AttributeFinder.get_instance_attributes_as_getters(
                node,
                attrs,
            )
            value = node_attributes['value']()
            children = node_attributes['children']()
        except AttributeNotFound:
            attrs_copy_without_children = dict()
            for key in attrs:
                if key != 'children':
                    attrs_copy_without_children[key] = attrs[key]

            node_attributes = AttributeFinder.get_instance_attributes_as_getters(
                node,
                attrs_copy_without_children,
            )
            value = node_attributes['value']()

            children_getter = AttributeFinder.merge_attributes_into_one_function(
                node,
                DEFAULT_BINARY_NODE_ATTRS,
                ['left', 'right']
            )
            children = children_getter()

        return value, children

    @staticmethod
    def get_block_width(node_name, child_widths):
        return max(len(node_name), sum(child_widths) + len(child_widths) - 1)

    @staticmethod
    def determine_midpoints(child_widths):
        child_midpoints = []
        child_end = 0
        for width in child_widths:
            child_midpoints.append(child_end + (width // 2))
            child_end += width + 1
        return child_midpoints

    @staticmethod
    def build_brace(display_width, child_midpoints):
        brace_builder = []
        for i in range(display_width):
            if i < child_midpoints[0] or i > child_midpoints[-1]:
                brace_builder.append(' ')
            elif i in child_midpoints:
                brace_builder.append('+')
            else:
                brace_builder.append('-')
        return ''.join(brace_builder)

    @staticmethod
    def stack_str_blocks(blocks):
        """
        Takes a list of multi-line strings, and stacks them horizontally.
    
        For example, given 'aaa\naaa' and 'bbbb\nbbbb', it returns
        'aaa bbbb\naaa bbbb'.  As in:
    
        'aaa  +  'bbbb   =  'aaa bbbb
         aaa'     bbbb'      aaa bbbb'
    
        Each block must be rectangular (all lines are the same length), but blocks
        can be different sizes.
    
        :param blocks: list of multi-line strings
        """
        builder = []
        block_lens = [TreeVisualisation.block_width(bl) for bl in blocks]
        split_blocks = [bl.split('\n') for bl in blocks]

        for line_list in itertools.zip_longest(*split_blocks, fillvalue=None):
            for i, line in enumerate(line_list):
                if line is None:
                    builder.append(' ' * block_lens[i])
                else:
                    builder.append(line)
                if i != len(line_list) - 1:
                    builder.append(' ')
            builder.append('\n')

        return ''.join(builder[:-1])

    @staticmethod
    def block_width(block):
        if '\n' in block:
            return block.index('\n')
        return len(block)
