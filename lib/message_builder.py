from .error_helpers import ERROR_HELPERS


class MessageBuilder:
    def __init__(self):
        self._commands = []
        self._main_message = ''
        self._help_message = ''
        self._error_message = ''
        self._test_name = ''

    def reset(self):
        self._commands = []
        self._main_message = ''
        self._help_message = ''
        self._error_message = ''

    def set_test_name(self, name):
        self._test_name = name

    def test_name(self):
        return self._test_name

    def set_error_message(self, error):
        self._error_message = self._format_error_message(error)

    def add_command(self, command, args=None):
        self._commands.append((command, args))

    def add_commands(self, *commands):
        for command in commands:
            self.add_command(command)

    def reset_commands(self):
        self._commands = []

    def set_help_message(self, message):
        self._help_message = message

    def set_main_message(self, message):
        self._main_message = message

    def _get_formatted_commands(self):
        title = self._format_title('Prikazy pred testom')
        formatted_commands = ''
        for record in self._commands:
            command, args = record
            command_with_args = command + '({})'
            if args:
                args_str = ''
                for arg in args:
                    if isinstance(arg, str):
                        arg = '\'{}\''.format(arg)
                    args_str += str(arg) + ', '
                command_with_args = command_with_args.format(args_str[:-2])
            else:
                command_with_args = command_with_args.format('')
            formatted_commands += command_with_args + '\n'

        return title + formatted_commands

    def _format_title(self, title):
        return '{}:\n-------\n'.format(title)

    def get_message(self):
        msg = ''

        if self._main_message:
            msg += self._format_title('Hlavny vypis')
            msg += str(self._main_message)
            msg += '\n\n'

        if self._commands:
            commands = self._get_formatted_commands()
            msg += commands
            msg += '\n\n'

        if self._help_message:
            msg += self._format_title('Pomocna sprava')
            msg += str(self._help_message)
            msg += '\n\n'

        if self._error_message:
            msg += self._format_title('Chybova hlaska')
            msg += str(self._error_message)
            msg += '\n\n'

        return msg

    def _format_error_message(self, error):
        if not isinstance(error, tuple) or len(error) != 2:
            return error

        error_message = error[1]
        error_message_lines = error_message.split('\n')
        main_error_line = error_message_lines[-1]
        for line in error_message_lines[::-1]:
            if main_error_line:
                break
            main_error_line = line

        error_type, *_ = main_error_line.split(':')

        msg = main_error_line
        if error_type in ERROR_HELPERS:
            msg += '\n\nO tomto errore:'
            msg += ERROR_HELPERS[error_type]

        return msg
