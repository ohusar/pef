"""
Stores default expected attributes for structures, that are supposed to be tested byt P.E.F.
module.
"""
from collections import Iterable


def format_all_defaults(all_defaults):
    for defaults in all_defaults:
        format_default_attributes(defaults)


def format_default_attributes(attributes_dictionary):
    for key in attributes_dictionary:
        attribute_names = attributes_dictionary[key]
        attributes_dictionary[key] = format_attribute_names(attribute_names)


def format_attribute_names(attribute_names):
    new_attribute_names = set()

    if not is_iterable(attribute_names):
        attribute_names = {attribute_names}

    try:
        attribute_names = set(attribute_names)
    except TypeError as e:
        raise TypeError('\'attribute_names\' shouldn\'t be iterable and not'
                        ' convertible to set.\nFailed with: {}'.format(e))

    for attribute_name in attribute_names:
        new_attribute_names.add(attribute_name)
        new_attribute_names.add('_{}'.format(attribute_name))

    return new_attribute_names


def is_iterable(var):
    string_types = (str, bytes)
    for t in string_types:
        if isinstance(var, t):
            return False
    return isinstance(var, Iterable)


DEFAULT_TREE_ATTRS = {
    'root': {'root', 'koren'},
}

DEFAULT_NODE_ATTRS = {
    'value': {'value', 'val', 'name', 'data', 'meno'},
    'children': {'ch', 'children', 'descendants', 'deti', 'potomkovia'},
}

DEFAULT_LINKED_LIST_ATTRS = {
    'start': {'root', 'koren', 'zac', 'zaciatok', 'start', 'beginning', 'beg', 'front'},
}

DEFAULT_LINKED_LIST_NODE_ATTRS = {
    'value': {'value', 'val', 'name', 'data', 'meno'},
    'next': {'next', 'dalsi', 'n', 'next_node', 'dalsi_vrchol'}
}

DEFAULT_BINARY_NODE_ATTRS = {
    'left': {'left', 'l', 'lavy', 'l'},
    'right': {'right', 'r', 'pravy', 'p'}
}

DEFAULT_STACK_ATTRS = {
    'data': {'data', 'pole'},
    'pop': {'pop', 'pop_back'}
}

DEFAULT_QUEUE_ATTRS = {
    'data': {'data', 'pole'},
    'dequeue': {'dequeue', 'pop', 'pop_front'}
}

ALL_DEFAULTS = (
    DEFAULT_TREE_ATTRS, DEFAULT_NODE_ATTRS, DEFAULT_BINARY_NODE_ATTRS, DEFAULT_STACK_ATTRS,
    DEFAULT_QUEUE_ATTRS
)

format_all_defaults(ALL_DEFAULTS)
