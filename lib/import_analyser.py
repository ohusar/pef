import os
import random
import string
from types import ModuleType
from importlib import import_module


class ImportAnalyser:
    empty_module_path = 'empty_module_path.py'

    @staticmethod
    def get_names_in_scope(filename):
        """
        Returns all imports and classes used in a file.

        :param filename: path to file as a string
        :return: set()
        """
        ImportAnalyser.generate_empty_module()
        empty_module_name = ImportAnalyser.empty_module_path.split('.')[0]
        empty_module_names_in_scope = set(dir(import_module(empty_module_name)))
        ImportAnalyser.delete_empty_module()

        module_name = filename.split('.')[0]
        module = import_module(module_name)
        names_in_module_scope = set(dir(module))

        names_in_scope = names_in_module_scope - empty_module_names_in_scope

        result = set()
        for name in names_in_scope:
            attr = getattr(module, name)
            if isinstance(attr, ModuleType):
                result.add(name)

        return result

    @staticmethod
    def generate_empty_module():
        i = 1
        # We do not want to overwrite any file
        while os.path.exists(ImportAnalyser.empty_module_path):
            ImportAnalyser.empty_module_path = ''
            for _ in range(i):
                ImportAnalyser.empty_module_path += random.choice(
                    string.ascii_uppercase + string.digits
                )
            ImportAnalyser.empty_module_path += '.py'
            i += 1
        empty_module = open(ImportAnalyser.empty_module_path, 'w')
        empty_module.write('#  this is an empty module')
        empty_module.close()

    @staticmethod
    def delete_empty_module():
        if os.path.exists(ImportAnalyser.empty_module_path):
            os.remove(ImportAnalyser.empty_module_path)
