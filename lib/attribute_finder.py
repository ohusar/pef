from .custom_exceptions import AttributeNotFound


class BadValue:
    """
    Class used for default values when retrieving attributes. None was not suitable.
    """
    def __bool__(self):
        return False

BAD_VALUE = BadValue()


class AttributeFinder:

    @staticmethod
    def get_instance_attributes_as_getters(instance, attributes):
        """
        Receives possible names for attributes and finds their actual names in instance, and
        transforms them to getter functions.

        :param instance: instance of class we search in
        :param attributes: dictionary that maps
            attr_name (str) -> possible_names_for_attribute (list)
        :return: dictionary mapping attr_name (str) -> attribute getter (callable)
        :raises: AttributeNotFound exception if instance does not have none of
            possible_names_for_attribute
        """

        result_dictionary = dict()
        for attribute_name in attributes:
            possible_names = attributes[attribute_name]

            actual_attribute = AttributeFinder.get_getter_for_attribute(instance, possible_names)

            result_dictionary[attribute_name] = actual_attribute

        return result_dictionary

    @staticmethod
    def get_getter_for_attribute(instance, possible_names):
        """
        Retrieves attribute from instance as getter.

        Ex:

        class Example:
            def __init__(self):
                self.example = 'example'

        example = Example()

        e = AttributeFinder.get_getter_for_attribute(example, ['e', 'example', ...])
        print(e()) -> will print 'example'

        :param instance: object that we want to extract from
        :param possible_names: possible names for the attribute
        :return: function that returns value of the attribute
        :raises AttributeNotFound if given instance doesn't have attribute from one of possible_names
        """
        def ensure_is_callable(attribute):
            if not callable(attribute):
                return lambda: attribute
            return attribute

        actual_attribute = BAD_VALUE
        for possible_name in possible_names:
            if actual_attribute == BAD_VALUE:
                actual_attribute = getattr(instance, possible_name, BAD_VALUE)

        if BAD_VALUE == actual_attribute:
            raise AttributeNotFound(
                '{} should have one of these attributes: {}.'.format(
                    type(instance),
                    possible_names,
                )
            )

        return ensure_is_callable(actual_attribute)

    @staticmethod
    def merge_attributes_into_one_function(instance, attributes, ordered_keys=None):
        """
        Merge attributes into one function, which will return list of the attributes. Does not
        include attributes, that return None. If provided with ordered keys, the list will be
        ordered as so.

        Example:

        node = Node(left='L', right='R')
        children = merge_attributes_into_one_function(
            node, {'left': ['left', '_left'], 'right': ['right, '_right']}
        )

        print(children())

        -> will print out

        ['L', 'R'] or ['R', 'L']

        But with ordered_keys=['left', 'right'] the output will be ['L', 'R']

        :param instance: instance of class we search in
        :param attributes: attributes: dictionary that maps
            attr_name (str) -> possible_names_for_attribute (list)
        :param ordered_keys: list of ordered keys
        :return: function that returns iterable of attributes parsed from attributes dict
        :raises: AttributeNotFound exception if instance does not have none of
            possible_names_for_attribute.
                 KeyError if key from ordered_keys is not present in param dict.
        """

        attribute_getters = AttributeFinder.get_instance_attributes_as_getters(
            instance,
            attributes,
        )

        result_list = []
        keys = ordered_keys if ordered_keys else attribute_getters.keys()
        for name in keys:
            getter = attribute_getters[name]
            value = getter()
            if value:
                result_list.append(value)

        return lambda: result_list

    @staticmethod
    def get_is_empty_method(instance):
        """
        Retrieves "is_empty" method from an instance. Also tries to use __len__ method and build
        the method, if it doesn't find it explicitly.

        :param instance: object that we want to extract
        :raise: AttributeNotFound if instance doesn't have __len__ and possible empty method names
        :return: is_empty method
        """
        try:
            is_empty_method = AttributeFinder.get_getter_for_attribute(
                instance, ['empty', 'is_empty', 'prazdny']
            )
        except AttributeNotFound as err:
            len_method = getattr(instance, '__len__', BAD_VALUE)
            is_empty_method = AttributeFinder.make_is_empty_from_len(len_method)

            if is_empty_method == BAD_VALUE:
                msg = 'Or, having __len__ method is acceptable too.'
                raise AttributeNotFound(str(err) + '\n{}'.format(msg))

        return is_empty_method

    @staticmethod
    def make_is_empty_from_len(len_method):
        """
        Creates is_empty method from __len__ by comparing it to 0.

        :param len_method: __len__ method of an object (or BadValue)
        :return: is_empty method or BadValue
        """
        if len_method != BAD_VALUE:
            return lambda: len_method() == 0
        return BAD_VALUE
