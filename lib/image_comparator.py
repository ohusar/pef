import os

from skimage.measure import compare_ssim as ssim
import numpy as np
import cv2


class ImageComparator:

    @staticmethod
    def mean_squared_error(image_a, image_b):
        """
        The 'Mean Squared Error' between the two images is the
        sum of the squared difference between the two images.
        NOTE: the two images must have the same dimension.
        """
        err = np.sum((image_a.astype("float") - image_b.astype("float")) ** 2)
        err /= float(image_a.shape[0] * image_a.shape[1])

        # return the MSE, the lower the error, the more "similar" the two images are
        return err

    @staticmethod
    def compare_images(image_a, image_b):
        """Compute the mean squared error and structural similarity index for the images."""
        m = ImageComparator.mean_squared_error(image_a, image_b)
        s = ssim(image_a, image_b, multichannel=True)

        return m, s

    @staticmethod
    def compute(path_a, path_b):

        image_a = ImageComparator.get_img_or_raise_not_found(path_a)
        image_b = ImageComparator.get_img_or_raise_not_found(path_b)

        return ImageComparator.compare_images(image_a, image_b)

    @staticmethod
    def get_img_or_raise_not_found(path):
        if os.path.isfile(path):
            return cv2.imread(path, 1)
        raise FileNotFoundError('File {} does not exist.'.format(path))
