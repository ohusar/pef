from importlib import util, import_module


class ClassStructureChecker:

    @staticmethod
    def check_class_structure(module_name, class_structure=None):
        if class_structure is None:
            class_structure = dict()

        module_is_not_importable = util.find_spec(module_name) is None
        if module_is_not_importable:
            return False

        module = import_module(module_name)
        is_missing_class = ClassStructureChecker._is_missing_class(module, class_structure)

        return not is_missing_class

    @staticmethod
    def _is_missing_class(cls, class_structure):
        result = False

        if isinstance(class_structure, list):
            return ClassStructureChecker._is_missing_attributes(cls, class_structure)

        for class_name in class_structure:
            attr = getattr(cls, class_name, None)
            if attr is None or not isinstance(attr, type):
                result = True

            result = result or ClassStructureChecker._is_missing_class(
                attr,
                class_structure[class_name]
            )

        return result

    @staticmethod
    def _is_missing_attributes(cls, attributes):
        for attr in attributes:
            try:
                getattr(cls, attr)
            except AttributeError:
                return True
        return False
