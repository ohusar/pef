import tkinter
from abc import ABC, abstractmethod

from PIL import Image as PILImage, ImageDraw, ImageFont


class CanvasObjectStore:

    def __init__(self):
        self.store = dict()

    def add(self, key, canvas_object):
        if self.has(key):
            raise KeyError('Object with this key already exists.')
        self.store[key] = canvas_object

    def remove(self, key):
        del self.store[key]

    def has(self, key):
        return key in self.store

    def move(self, key, *args, **kwargs):
        self.store[key].move(*args, **kwargs)

    def draw(self, draw):
        for key in sorted(self.store):
            self.store[key].draw(draw)


class CanvasObject(ABC):

    def __init__(self, xy, **kwargs):
        self.xy = list(xy)
        self.width = kwargs.get('width', 0)
        self.fill = kwargs.get('fill', 'black')
        self.outline = kwargs.get('outline', None)
        self.text = kwargs.get('text', None)

    def move(self, *args):
        delta_x, delta_y = args[0], args[1]
        for i in range(len(self.xy)):
            if i % 2 == 0:
                self.xy[i] += delta_x
            else:
                self.xy[i] += delta_y

    @abstractmethod
    def draw(self, draw):
        pass


class Line(CanvasObject):

    def draw(self, draw):
        if len(self.xy) == 4:
            draw.line(self.xy, fill=self.fill, width=self.width + 1)
        else:
            start = 0
            end = 4
            while end < len(self.xy):
                x1, y1, x2, y2 = self.xy[start:end]
                draw.line((x1, y1, x2, y2), fill=self.fill, width=self.width + 1)
                start += 2
                end += 2


class Ellipse(CanvasObject):

    def draw(self, draw):
        draw.ellipse(self.xy, fill=self.fill, outline=self.outline)


class Rectangle(CanvasObject):

    def draw(self, draw):
        draw.rectangle(self.xy, fill=self.fill, outline=self.outline)


class Polygon(CanvasObject):

    def draw(self, draw):
        draw.polygon(self.xy, fill=self.fill, outline=self.outline)


class Text(CanvasObject):

    def draw(self, draw):
        draw.text(
            self.xy,
            text=self.text,
            fill=self.fill,
            font=ImageFont.truetype('OTF/Crimson-Roman.otf', size=15)
        )


class Image(CanvasObject):

    def __init__(self, xy, **kwargs):
        super().__init__(xy, **kwargs)
        self.image = kwargs.get('image', None)

    def draw(self, draw):
        x, y = self.xy[0], self.xy[1]

        img_w, img_h = self.image.size
        offset = ((x - img_w) // 2, (y - img_h) // 2)

        draw.paste(self.image, offset)


class Canvas(tkinter.Canvas):

    def __init__(self, master=None, cnf=None, controlled=False, **kw):
        """
        Valid resource names: background, bd, bg, borderwidth, closeenough,
        confine, cursor, height, highlightbackground, highlightcolor,
        highlightthickness, insertbackground, insertborderwidth,
        insertofftime, insertontime, insertwidth, offset, relief,
        scrollregion, selectbackground, selectborderwidth, selectforeground,
        state, takefocus, width, xscrollcommand, xscrollincrement,
        yscrollcommand, yscrollincrement.
        """

        if cnf is None:
            cnf = {}

        super().__init__(master, cnf, **kw)
        self.bg = kw.get('bg', 'white')
        self.width = kw.get('width', 1)
        self.height = kw.get('height', 1)
        self.store = CanvasObjectStore()
        self.controlled = controlled

    def _extract(self):
        if not self.controlled:
            self.print()

    def move(self, *args):
        super().move(*args)
        self.store.move(args[0], *args[1:])
        self._extract()

    def delete(self, *args):
        super().delete(*args)
        for i in args:
            self.store.remove(i)
        self._extract()

    def create_line(self, *args, **kw):
        key = super().create_line(*args, **kw)

        canvas_object = Line(args, **kw)
        self.store.add(key, canvas_object)
        self._extract()

        return key

    def create_oval(self, *args, **kw):
        key = super().create_oval(*args, **kw)

        canvas_object = Ellipse(args, **kw)
        self.store.add(key, canvas_object)
        self._extract()

        return key

    def create_polygon(self, *args, **kw):
        key = super().create_polygon(*args, **kw)

        canvas_object = Polygon(args, **kw)
        self.store.add(key, canvas_object)
        self._extract()

        return key

    def create_rectangle(self, *args, **kw):
        key = super().create_rectangle(*args, **kw)

        canvas_object = Rectangle(args, **kw)
        self.store.add(key, canvas_object)
        self._extract()

        return key

    def create_text(self, *args, **kw):
        kw['anchor'] = 'nw'
        key = super().create_text(*args, **kw)

        canvas_object = Text(args, **kw)
        self.store.add(key, canvas_object)
        self._extract()

        return key

    def create_image(self, *args, **kw):
        key = super().create_image(*args, **kw)

        canvas_object = Image(args, **kw)
        self.store.add(key, canvas_object)
        self._extract()

        return key

    def extract_image(self, filename):
        image = PILImage.new("RGB", (self.width, self.height), (255, 255, 255))
        draw = ImageDraw.Draw(image)

        draw.rectangle((-1, -1, self.width + 1, self.height + 1), fill=self.bg)
        self.store.draw(draw)

        image.save(filename)

    def print(self):
        self.extract_image('output.png')
