"""
Copied from official documentation for v3.6.1.

Can be found in:
https://docs.python.org/3/library/exceptions.html
"""

ERROR_HELPERS = {
    'AssertionError':
        """
Raised when an assert statement fails.
""",

    'AttributeError':
        """
Raised when an attribute reference (see Attribute references) or assignment fails. (When an object does not support attribute references or attribute assignments at all, TypeError is raised.)
""",

    'EOFError':
        """
Raised when the input() function hits an end-of-file condition (EOF) without reading any data. (N.B.: the io.IOBase.read() and io.IOBase.readline() methods return an empty string when they hit EOF.)
""",

    'FloatingPointError':
        """
Raised when a floating point operation fails. This exception is always defined, but can only be raised when Python is configured with the --with-fpectl option, or the WANT_SIGFPE_HANDLER symbol is defined in the pyconfig.h file.
""",

    'GeneratorExit':
        """
Raised when a generator or coroutine is closed; see generator.close() and coroutine.close(). It directly inherits from BaseException instead of Exception since it is technically not an error.
""",

    'ImportError':
        """
Raised when the import statement has troubles trying to load a module. Also raised when the “from list” in from ... import has a name that cannot be found.

The name and path attributes can be set using keyword-only arguments to the constructor. When set they represent the name of the module that was attempted to be imported and the path to any file which triggered the exception, respectively.
""",

    'ModuleNotFoundError':
        """
A subclass of ImportError which is raised by import when a module could not be located. It is also raised when None is found in sys.modules.
""",

    'IndexError':
        """
Raised when a sequence subscript is out of range. (Slice indices are silently truncated to fall in the allowed range; if an index is not an integer, TypeError is raised.)
""",

    'KeyError':
        """
Raised when a mapping (dictionary) key is not found in the set of existing keys.
""",

    'KeyboardInterrupt':
        """
Raised when the user hits the interrupt key (normally Control-C or Delete). During execution, a check for interrupts is made regularly. The exception inherits from BaseException so as to not be accidentally caught by code that catches Exception and thus prevent the interpreter from exiting.
""",

    'MemoryError':
        """
Raised when an operation runs out of memory but the situation may still be rescued (by deleting some objects). The associated value is a string indicating what kind of (internal) operation ran out of memory. Note that because of the underlying memory management architecture (C’s malloc() function), the interpreter may not always be able to completely recover from this situation; it nevertheless raises an exception so that a stack traceback can be printed, in case a run-away program was the cause.
""",

    'NameError':
        """
Raised when a local or global name is not found. This applies only to unqualified names. The associated value is an error message that includes the name that could not be found.
""",

    'NotImplementedError':
        """
This exception is derived from RuntimeError. In user defined base classes, abstract methods should raise this exception when they require derived classes to override the method, or while the class is being developed to indicate that the real implementation still needs to be added.
""",

    'OSError([arg])':
        """
This exception is raised when a system function returns a system-related error, including I/O failures such as “file not found” or “disk full” (not for illegal argument types or other incidental errors).
""",

    'OverflowError':
        """
Raised when the result of an arithmetic operation is too large to be represented. This cannot occur for integers (which would rather raise MemoryError than give up). However, for historical reasons, OverflowError is sometimes raised for integers that are outside a required range. Because of the lack of standardization of floating point exception handling in C, most floating point operations are not checked.
""",

    'RecursionError':
        """
This exception is derived from RuntimeError. It is raised when the interpreter detects that the maximum recursion depth (see sys.getrecursionlimit()) is exceeded.
""",

    'ReferenceError':
        """
This exception is raised when a weak reference proxy, created by the weakref.proxy() function, is used to access an attribute of the referent after it has been garbage collected. For more information on weak references, see the weakref module.
""",

    'RuntimeError':
        """
Raised when an error is detected that doesn’t fall in any of the other categories. The associated value is a string indicating what precisely went wrong.
""",

    'StopIteration':
        """
Raised by built-in function next() and an iterator‘s __next__() method to signal that there are no further items produced by the iterator.

The object has a single attribute value, which is given as an argument when constructing the exception, and defaults to None.

When a generator or coroutine function returns, a new StopIteration instance is raised, and the value returned by the function is used as the value parameter to the constructor of the exception.
""",

    'StopAsyncIteration':
        """
Must be raised by __anext__() method of an asynchronous iterator object to stop the iteration.
""",

    'SyntaxError':
        """
Raised when the parser encounters a syntax error. This may occur in an import statement, in a call to the built-in functions exec() or eval(), or when reading the initial script or standard input (also interactively).

Instances of this class have attributes filename, lineno, offset and text for easier access to the details. str() of the exception instance returns only the message.
""",

    'IndentationError':
        """
Base class for syntax errors related to incorrect indentation. This is a subclass of SyntaxError.
""",

    'TabError':
        """
Raised when indentation contains an inconsistent use of tabs and spaces. This is a subclass of IndentationError.
""",

    'SystemError':
        """
Raised when the interpreter finds an internal error, but the situation does not look so serious to cause it to abandon all hope. The associated value is a string indicating what went wrong (in low-level terms).

You should report this to the author or maintainer of your Python interpreter. Be sure to report the version of the Python interpreter (sys.version; it is also printed at the start of an interactive Python session), the exact error message (the exception’s associated value) and if possible the source of the program that triggered the error.
""",

    'SystemExit':
        """
This exception is raised by the sys.exit() function. It inherits from BaseException instead of Exception so that it is not accidentally caught by code that catches Exception. This allows the exception to properly propagate up and cause the interpreter to exit. When it is not handled, the Python interpreter exits; no stack traceback is printed. The constructor accepts the same optional argument passed to sys.exit(). If the value is an integer, it specifies the system exit status (passed to C’s exit() function); if it is None, the exit status is zero; if it has another type (such as a string), the object’s value is printed and the exit status is one.

A call to sys.exit() is translated into an exception so that clean-up handlers (finally clauses of try statements) can be executed, and so that a debugger can execute a script without running the risk of losing control. The os._exit() function can be used if it is absolutely positively necessary to exit immediately (for example, in the child process after a call to os.fork()).

code
The exit status or error message that is passed to the constructor. (Defaults to None.)
""",

    'TypeError':
        """
Raised when an operation or function is applied to an object of inappropriate type. The associated value is a string giving details about the type mismatch.

This exception may be raised by user code to indicate that an attempted operation on an object is not supported, and is not meant to be. If an object is meant to support a given operation but has not yet provided an implementation, NotImplementedError is the proper exception to raise.

Passing arguments of the wrong type (e.g. passing a list when an int is expected) should result in a TypeError, but passing arguments with the wrong value (e.g. a number outside expected boundaries) should result in a ValueError.
""",

    'UnboundLocalError':
        """
Raised when a reference is made to a local variable in a function or method, but no value has been bound to that variable. This is a subclass of NameError.
""",

    'UnicodeError':
        """
Raised when a Unicode-related encoding or decoding error occurs. It is a subclass of ValueError.

UnicodeError has attributes that describe the encoding or decoding error. For example, err.object[err.start:err.end] gives the particular invalid input that the codec failed on.

encoding
The name of the encoding that raised the error.

reason
A string describing the specific codec error.

object
The object the codec was attempting to encode or decode.

start
The first index of invalid data in object.

end
The index after the last invalid data in object.
""",

    'UnicodeEncodeError':
        """
Raised when a Unicode-related error occurs during encoding. It is a subclass of UnicodeError.
""",

    'UnicodeDecodeError':
        """
Raised when a Unicode-related error occurs during decoding. It is a subclass of UnicodeError.
""",

    'UnicodeTranslateError':
        """
Raised when a Unicode-related error occurs during translating. It is a subclass of UnicodeError.
""",

    'ValueError':
        """
Raised when a built-in operation or function receives an argument that has the right type but an inappropriate value, and the situation is not described by a more precise exception such as IndexError.
""",

    'ZeroDivisionError':
        """
Raised when the second argument of a division or modulo operation is zero. The associated value is a string indicating the type of the operands and the operation.
""",

    'BlockingIOError':
        """
Raised when an operation would block on an object (e.g. socket) set for non-blocking operation. Corresponds to errno EAGAIN, EALREADY, EWOULDBLOCK and EINPROGRESS.

In addition to those of OSError, BlockingIOError can have one more attribute:

characters_written
An integer containing the number of characters written to the stream before it blocked. This attribute is available when using the buffered I/O classes from the io module.
""",

    'ChildProcessError':
        """
Raised when an operation on a child process failed. Corresponds to errno ECHILD.
""",

    'ConnectionError':
        """
A base class for connection-related issues.

Subclasses are BrokenPipeError, ConnectionAbortedError, ConnectionRefusedError and ConnectionResetError.
""",

    'BrokenPipeError':
        """
A subclass of ConnectionError, raised when trying to write on a pipe while the other end has been closed, or trying to write on a socket which has been shutdown for writing. Corresponds to errno EPIPE and ESHUTDOWN.
""",

    'ConnectionAbortedError':
        """
A subclass of ConnectionError, raised when a connection attempt is aborted by the peer. Corresponds to errno ECONNABORTED.
""",

    'ConnectionRefusedError':
        """
A subclass of ConnectionError, raised when a connection attempt is refused by the peer. Corresponds to errno ECONNREFUSED.
""",

    'ConnectionResetError':
        """
A subclass of ConnectionError, raised when a connection is reset by the peer. Corresponds to errno ECONNRESET.
""",

    'FileExistsError':
        """
Raised when trying to create a file or directory which already exists. Corresponds to errno EEXIST.
""",

    'FileNotFoundError':
        """
Raised when a file or directory is requested but doesn’t exist. Corresponds to errno ENOENT.
""",

    'InterruptedError':
        """
Raised when a system call is interrupted by an incoming signal. Corresponds to errno EINTR.

Python retries system calls when a syscall is interrupted by a signal, except if the signal handler raises an exception (see PEP 475 for the rationale), instead of raising InterruptedError.
""",

    'IsADirectoryError':
        """
Raised when a file operation (such as os.remove()) is requested on a directory. Corresponds to errno EISDIR.
""",

    'NotADirectoryError':
        """
Raised when a directory operation (such as os.listdir()) is requested on something which is not a directory. Corresponds to errno ENOTDIR.
""",

    'PermissionError':
        """
Raised when trying to run an operation without the adequate access rights - for example filesystem permissions. Corresponds to errno EACCES and EPERM.
""",

    'ProcessLookupError':
        """
Raised when a given process doesn’t exist. Corresponds to errno ESRCH.
""",

    'TimeoutError':
        """
Raised when a system function timed out at the system level. Corresponds to errno ETIMEDOUT.
""",

    'Warning':
        """
Base class for warning categories.
""",

    'UserWarning':
        """
Base class for warnings generated by user code.
""",

    'DeprecationWarning':
        """
Base class for warnings about deprecated features.
""",

    'PendingDeprecationWarning':
        """
Base class for warnings about features which will be deprecated in the future.
""",

    'SyntaxWarning':
        """
Base class for warnings about dubious syntax.
""",

    'RuntimeWarning':
        """
Base class for warnings about dubious runtime behavior.
""",

    'FutureWarning':
        """
Base class for warnings about constructs that will change semantically in the future.
""",

    'ImportWarning':
        """
Base class for warnings about probable mistakes in module imports.
""",

    'UnicodeWarning':
        """
Base class for warnings related to Unicode.
""",

    'BytesWarning':
        """
Base class for warnings related to bytes and bytearray.
""",

    'ResourceWarning':
        """
Base class for warnings related to resource usage.
"""
}
