import keyword
import tokenize
from io import BytesIO


class KeywordAnalyser:

    @staticmethod
    def get_keywords_used(filename):
        """
        Returns all keywords used in a file.

        :param filename: path to file as a string
        :return: set()
        """
        keywords_used = set()
        tokenized_generator = KeywordAnalyser.tokenize_file(filename)
        for toknum, tokval, _, _, _ in tokenized_generator:
            if toknum == tokenize.NAME and keyword.iskeyword(tokval):
                keywords_used.add(tokval)
        return keywords_used

    @staticmethod
    def tokenize_file(filename, encoding='utf-8'):
        with open(filename) as file:
            file_content = file.read()
            # tokenize argument must be a callable object which provides the
            # same interface as the io.IOBase.readline()

            tokenized_content = tokenize.tokenize(
                BytesIO(file_content.encode(encoding)).readline
            )

            return tokenized_content
