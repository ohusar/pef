import base64
import unittest
from copy import deepcopy

from .lib.attribute_finder import AttributeFinder
from .lib.custom_exceptions import AttributeNotFound
from .lib.default_structure_attributes import *
from .lib.image_comparator import ImageComparator
from .lib.message_builder import MessageBuilder
from .lib.tree_visualisation import TreeVisualisation


class TestCase(unittest.TestCase):
    longMessage = False

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.message = MessageBuilder()
        self.points = 0

    def set_test_name(self, name):
        self.message.set_test_name(name)

    def get_test_name(self):
        return self.message.test_name()

    def add_command(self, command, args):
        self.message.add_command(command, args)

    def add_commands(self, commands):
        self.message.add_commands(commands)

    def reset_commands(self):
        self.message.reset_commands()

    def set_main_message(self, message):
        self.message.set_main_message(message)

    def assertImageEqual(self, path_expected, path_actual):
        mse, ssim = ImageComparator.compute(path_expected, path_actual)

        with open(path_expected, 'rb') as expected_file:
            base64_expected = base64.b64encode(expected_file.read())
        with open(path_actual, 'rb') as actual_file:
            base64_actual = base64.b64encode(actual_file.read())

        msg = 'Images do not match. Here is what they look like:' \
              '<br>' \
              '<p>Expected output</p>' \
              '<img src="data:image/png;base64,{}"/>' \
              '<p>Actual output</p>' \
              '<img src="data:image/png;base64,{}"/>'

        self.message.set_help_message(
            msg.format(base64_expected.decode(), base64_actual.decode())
        )

        self.assertEqual(1.0, ssim, msg.format(base64_expected.decode(), base64_actual.decode()))
        self.assertEqual(0.0, mse, msg.format(base64_expected.decode(), base64_actual.decode()))

    def assertImageAlmostEqual(self, path_expected, path_actual, threshold):
        mse, ssim = ImageComparator.compute(path_expected, path_actual)

        with open(path_expected, 'rb') as expected_file:
            base64_expected = base64.b64encode(expected_file.read())
        with open(path_actual, 'rb') as actual_file:
            base64_actual = base64.b64encode(actual_file.read())

        msg = 'Images do not match. Here is what they look like:' \
              '<br>' \
              '<p>Expected output</p>' \
              '<img src="data:image/png;base64,{}"/>' \
              '<p>Actual output</p>' \
              '<img src="data:image/png;base64,{}"/>'

        formatted_msg = msg.format(base64_expected.decode(), base64_actual.decode())
        self.message.set_help_message(formatted_msg)

        self.assertAlmostEqual(
            0.0,
            mse,
            formatted_msg,
            delta=threshold
        )

    def assertFileContentEqualTo(self, file_path, to):
        file_content = self._read_file_or_raise(file_path)
        self.assertEqual(to, file_content)

    def assertFileContentEqual(self, expected_file_path, actual_file_path):
        expected_file_content = self._read_file_or_raise(expected_file_path)
        actual_file_content = self._read_file_or_raise(actual_file_path)
        self.assertEqual(expected_file_content, actual_file_content)

    def _read_file_or_raise(self, file_path):
        try:
            file = open(file_path)
        except FileNotFoundError as err:
            self.message.set_help_message('File "{}" does not exist.'.format(file_path))
            raise err
        file_content = file.read()
        file.close()
        return file_content

    def assertTreeEqual(self, expected, actual, attrs=DEFAULT_TREE_ATTRS, print_tree=False, max_depth=None):
        expected_attributes = AttributeFinder.get_instance_attributes_as_getters(expected, attrs)
        expected_root = expected_attributes['root']()

        actual_attributes = AttributeFinder.get_instance_attributes_as_getters(actual, attrs)
        actual_root = actual_attributes['root']()

        self.assertNodeEqual(expected_root, actual_root, print_tree=print_tree, max_depth=max_depth)
        self.message.set_help_message('')

    def assertNodeEqual(self, expected, actual, attrs=DEFAULT_NODE_ATTRS, print_tree=False, max_depth=None):
        if expected is None and actual is None:
            return
        nodes_are_equal = self._check_node_equal(expected, actual, attrs)

        if not nodes_are_equal:
            msg = 'Nodes are not equal.'
            if print_tree:
                msg = self.getNodeASCIIComparison(expected, actual, max_depth=max_depth)
            self.message.set_help_message(msg)
            raise AssertionError(msg)

    def getNodeASCIIComparison(self, expected, actual, max_depth=None):
        """Nodes should have children attribute/method."""

        msg = '\n\nExpected node:\n\n{}\n\nActual node:\n\n{}'
        expected_ascii = TreeVisualisation.ascii_representation(expected, max_depth=max_depth)
        actual_ascii = TreeVisualisation.ascii_representation(actual, max_depth=max_depth)

        expected_ascii = self._format_if_empty(expected_ascii)
        actual_ascii = self._format_if_empty(actual_ascii)

        msg = msg.format(expected_ascii, actual_ascii)
        return msg

    def assertLinkedListEqual(self, expected, actual, attrs=DEFAULT_LINKED_LIST_ATTRS):
        expected_attributes = AttributeFinder.get_instance_attributes_as_getters(expected, attrs)
        expected_start = expected_attributes['start']()

        actual_attributes = AttributeFinder.get_instance_attributes_as_getters(actual, attrs)
        actual_start = actual_attributes['start']()

        self.assertLinkedListNodeEqual(expected_start, actual_start)
        self.message.set_help_message('')

    def assertLinkedListNodeEqual(self, expected, actual, attrs=DEFAULT_LINKED_LIST_NODE_ATTRS):
        expected_data = self._extract_data_from_linked_list(expected, attrs)
        actual_data = self._extract_data_from_linked_list(actual, attrs)

        self.assertEqual(expected_data, actual_data)

    def assertInLinkedList(self, element, container, attrs=DEFAULT_LINKED_LIST_ATTRS):
        attributes = AttributeFinder.get_instance_attributes_as_getters(container, attrs)
        start = attributes['start']()

        linked_list_data = self._extract_data_from_linked_list(start, attrs)

        self.assertIn(element, linked_list_data)

    def assertStackEqual(self, expected, actual, attrs=DEFAULT_STACK_ATTRS):
        expected_data = self._extract_data_from_methods(
            expected, attrs['data'], attrs['pop'], True
        )
        actual_data = self._extract_data_from_methods(
            actual, attrs['data'], attrs['pop'], True
        )
        self.assertEqual(expected_data, actual_data)

    def assertInStack(self, element, container, attrs=DEFAULT_STACK_ATTRS):
        stack_data = self._extract_data_from_methods(
            container, attrs['data'], attrs['pop'], True
        )
        self.assertIn(element, stack_data)

    def assertQueueEqual(self, expected, actual, attrs=DEFAULT_QUEUE_ATTRS):
        possible_names = DEFAULT_QUEUE_ATTRS['dequeue']
        expected_data = self._extract_data_from_methods(expected, attrs['data'], possible_names)
        actual_data = self._extract_data_from_methods(actual, attrs['data'], possible_names)

        self.assertEqual(expected_data, actual_data)

    def assertInQueue(self, element, container, attrs=DEFAULT_STACK_ATTRS):
        actual_internal_data = AttributeFinder.get_getter_for_attribute(container, attrs['data'])
        self.assertIn(element, actual_internal_data)

    def assertEqualResultsForMethods(self, expected, actual, *methods, log_commands=True):
        for sample in methods:
            if log_commands:
                self.reset_commands()

            method = ''
            args = None
            if isinstance(sample, str):
                method = sample
            elif isinstance(sample, tuple) or isinstance(sample, list):
                method = sample[0]
                args = sample[1:]

            expected_desired_method = getattr(expected, method, None)
            actual_desired_method = getattr(actual, method, None)

            if log_commands:
                name = self._get_repr_or_name(actual, 'object_without_repr_method')
                self.add_command('{}.{}'.format(name, method), args=args)
            if args:
                expected_result = expected_desired_method(*args)
                actual_result = actual_desired_method(*args)
            else:
                expected_result = expected_desired_method()
                actual_result = actual_desired_method()

            self.message.set_help_message(
                'Chybný výsledok metódy {}'
                '\nExpected: {} vs. Actual: {}'.format(
                    method,
                    expected_result,
                    actual_result
                )
            )
            self.assertEqual(expected_result, actual_result)
            self.message.set_help_message('')

    def _check_node_equal(self, expected, actual, attrs):

        none_equality = self._check_none_equality(expected, actual)
        if none_equality is not None:
            return none_equality

        expected_value, expected_children = TreeVisualisation._get_expected_node_attributes(
            expected,
            attrs
        )
        actual_value, actual_children = TreeVisualisation._get_expected_node_attributes(
            actual,
            attrs
        )

        if expected_value != actual_value:
            return False

        if (not expected_children and actual_children) or \
                (expected_children and not actual_children):
            return False

        if not expected_children and not actual_children:
            return True

        if len(expected_children) != len(actual_children):
            return False

        for i in range(len(expected_children)):
            result = self._check_node_equal(expected_children[i], actual_children[i], attrs)
            if not result:
                return False
        return True

    def _extract_data_from_linked_list(self, instance, attrs):
        methods = AttributeFinder.get_instance_attributes_as_getters(instance, attrs)
        result = []
        while methods['next']():
            result.append(methods['value']())
            methods = AttributeFinder.get_instance_attributes_as_getters(
                methods['next'](),
                attrs
            )
        result.append(methods['value']())
        return result

    def _extract_data_from_methods(self, instance, attrs, possible_names, reverse=False):
        result = []
        try:
            expected_data = AttributeFinder.get_getter_for_attribute(instance, attrs)
            result = expected_data()
            if reverse:
                result = result[::-1]
        except AttributeNotFound:
            instance = deepcopy(instance)
            extract_method = AttributeFinder.get_getter_for_attribute(instance, possible_names)
            is_empty_method = AttributeFinder.get_is_empty_method(instance)
            while not is_empty_method():
                result.append(extract_method())
        return result

    def _check_none_equality(self, expected, actual):
        if expected is None and actual is None:
            return True
        if expected is None and actual is not None:
            return False
        if expected is not None and actual is None:
            return False
        return None

    def _format_if_empty(self, string):
        if string == '':
            return '\'\''
        return string

    def _get_repr_or_name(self, instance, name):
        if type(instance).__repr__ is not object.__repr__:
            return repr(instance)
        else:
            return name

    def _append_help_message(self, msg, new_line_content):
        help_message = ''
        if msg:
            help_message += '{}\n'.format(msg)
        help_message += new_line_content

        return help_message

    # PURELY OVERRIDDEN METHODS

    def assertEqual(self, first, second, msg=None):
        help_message = self._append_help_message(msg, 'Expected: {}\nActual: {}')
        self.message.set_help_message(help_message.format(first, second))
        super().assertEqual(first, second, msg)
        self.message.set_help_message('')

    def assertNotEqual(self, first, second, msg=None):
        help_message = self._append_help_message(msg, 'Both expected and actual are equal: {}')
        self.message.set_help_message(help_message.format(first))
        super().assertNotEqual(first, second, msg=msg)
        self.message.set_help_message('')

    def assertTrue(self, expr, msg=None):
        help_message = self._append_help_message(msg, '{} should be True')
        self.message.set_help_message(help_message.format(expr))
        super().assertTrue(expr, msg=msg)
        self.message.set_help_message('')

    def assertFalse(self, expr, msg=None):
        help_message = self._append_help_message(msg, '{} should be False')
        self.message.set_help_message(help_message.format(expr))
        super().assertFalse(expr, msg=msg)
        self.message.set_help_message('')

    def assertIs(self, expr1, expr2, msg=None):
        help_message = self._append_help_message(msg, 'Expression {} is not {}')
        self.message.set_help_message(help_message.format(expr1, expr2))
        super().assertIs(expr1, expr2, msg=msg)
        self.message.set_help_message('')

    def assertIsNot(self, expr1, expr2, msg=None):
        help_message = self._append_help_message(msg, 'Expression {} should not be {}')
        self.message.set_help_message(help_message.format(expr1, expr2))
        super().assertIsNot(expr1, expr2, msg=msg)
        self.message.set_help_message('')

    def assertIsNone(self, obj, msg=None):
        help_message = self._append_help_message(msg, '{} should be None')
        self.message.set_help_message(help_message.format(obj))
        super().assertIsNone(obj, msg=msg)
        self.message.set_help_message('')

    def assertIsNotNone(self, obj, msg=None):
        help_message = self._append_help_message(msg, '{} should not be None')
        self.message.set_help_message(help_message.format(obj))
        super().assertIsNotNone(obj, msg=msg)
        self.message.set_help_message('')

    def assertIn(self, member, container, msg=None):
        help_message = self._append_help_message(msg, 'Element: {} should be in container: {}')
        self.message.set_help_message(help_message.format(member, container))
        super().assertIn(member, container, msg=msg)
        self.message.set_help_message('')

    def assertNotIn(self, member, container, msg=None):
        help_message = self._append_help_message(msg, 'Element: {} should not be in container: {}')
        self.message.set_help_message(help_message.format(member, container))
        super().assertNotIn(member, container, msg=msg)
        self.message.set_help_message('')

    def assertIsInstance(self, obj, cls, msg=None):
        help_message = self._append_help_message(msg, 'Object {} should be of instance {}')
        self.message.set_help_message(help_message.format(obj, cls))
        super().assertIsInstance(obj, cls, msg=msg)
        self.message.set_help_message('')

    def assertNotIsInstance(self, obj, cls, msg=None):
        help_message = self._append_help_message(msg, 'Object {} should not be of instance {}')
        self.message.set_help_message(help_message.format(obj, cls))
        super().assertNotIsInstance(obj, cls, msg=msg)
        self.message.set_help_message('')

    def assertAlmostEqual(self, first, second, places=None, msg=None,
                          delta=None):
        help_message = self._append_help_message(
            msg, 'Elements {} and {} are not almost equal with delta: {} and places: {}'
        )
        self.message.set_help_message(help_message.format(first, second, places, delta))
        super().assertAlmostEqual(first, second, places=places, msg=msg, delta=delta)
        self.message.set_help_message('')

    def assertNotAlmostEqual(self, first, second, places=None, msg=None,
                             delta=None):
        help_message = self._append_help_message(
            msg, 'Elements {} and {} should not be almost equal with delta: {} and places: {}'
        )
        self.message.set_help_message(help_message.format(first, second, places, delta))
        super().assertNotAlmostEqual(first, second, places=places, msg=msg, delta=delta)
        self.message.set_help_message('')

    def assertGreater(self, a, b, msg=None):
        help_message = self._append_help_message(msg, '{} should be greater to {}')
        self.message.set_help_message(help_message.format(a, b))
        super().assertGreater(a, b, msg=msg)
        self.message.set_help_message('')

    def assertGreaterEqual(self, a, b, msg=None):
        help_message = self._append_help_message(msg, '{} should be greater or equal to {}')
        self.message.set_help_message(help_message.format(a, b))
        super().assertGreaterEqual(a, b, msg=msg)
        self.message.set_help_message('')

    def assertLess(self, a, b, msg=None):
        help_message = self._append_help_message(msg, '{} should be less to {}')
        self.message.set_help_message(help_message.format(a, b))
        super().assertLess(a, b, msg=msg)
        self.message.set_help_message('')

    def assertLessEqual(self, a, b, msg=None):
        help_message = self._append_help_message(msg, '{} should be less or equal to {}')
        self.message.set_help_message(help_message.format(a, b))
        super().assertLessEqual(a, b, msg=msg)
        self.message.set_help_message('')

    def assertRegex(self, text, expected_regex, msg=None):
        help_message = self._append_help_message(msg, 'Regex: {} doesn\'t match text: {}')
        self.message.set_help_message(help_message.format(expected_regex, text))
        super().assertRegex(text, expected_regex, msg=msg)
        self.message.set_help_message('')

    def assertNotRegex(self, text, unexpected_regex, msg=None):
        help_message = self._append_help_message(msg, 'Regex: {} should not match text: {}')
        self.message.set_help_message(help_message.format(unexpected_regex, text))
        super().assertNotRegex(text, unexpected_regex, msg=msg)
        self.message.set_help_message('')

    def assertCountEqual(self, first, second, msg=None):
        help_message = self._append_help_message(msg, 'Count of {} and {} should equal')
        self.message.set_help_message(help_message.format(first, second))
        super().assertCountEqual(first, second, msg=msg)
        self.message.set_help_message('')
