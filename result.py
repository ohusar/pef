import time
import sys
import unittest


class TestResult(unittest.TextTestResult):
    def __init__(self, stream, descriptions, _):
        super().__init__(stream, descriptions, 0)
        self.test_output = ''
        self.prerequisites_output = ''
        self.start_time = time.time()

        self.test_number = 0
        self.points = 0

        self._old_errors_len = 0
        self._old_failures_len = 0

    def stopTest(self, test):
        super().stopTest(test)
        self.add_test_result(test)

    def _determine_fail_type(self):
        fail_type = 'UNKNOWN'
        if len(self.errors) > self._old_errors_len:
            fail_type = 'ERROR'
        if len(self.failures) > self._old_failures_len:
            fail_type = 'FAIL'

        self._update_lengths()

        return fail_type

    def _update_lengths(self):
        self._old_errors_len = len(self.errors)
        self._old_failures_len = len(self.failures)

    def add_test_result(self, test):
        self.test_number += 1
        if test._outcome.success:
            self.test_output += 'OK: TEST {:2}'.format(self.test_number)
            if test.get_test_name():
                self.test_output += ': {}'.format(test.get_test_name())
            self.test_output += '\n'
            self.points += test.points
        else:
            fail_type = self._determine_fail_type()
            if fail_type == 'ERROR':
                test.message.set_error_message(self.errors[-1])

            self.test_output += '{}: TEST {:2}'.format(fail_type, self.test_number)
            if test.get_test_name():
                self.test_output += ': {}'.format(test.get_test_name())
            self.test_output += '\n===============================\n'
            self.test_output += test.message.get_message()
            self.test_output += '\n===============================\n'

    def check_prerequisites(self, runner):

        if runner.has_disallowed_imports:
            self.prerequisites_output += runner.imports_msg + '\n'
            self.points = 0

        if runner.is_missing_class:
            self.prerequisites_output += runner.class_msg + '\n'
            self.points = 0

        if runner.has_disallowed_keywords:
            self.prerequisites_output += runner.keywords_msg + '\n'
            self.points = 0

        self.test_output += '\n\nCode style review:\n{}'.format(runner.pep_msg)

    def print_outcome(self, to=sys.stderr):
        self.stop_time = time.time()
        time_taken = '%.3fs' % (self.stop_time - self.start_time)
        print(
            'Ran {} tests with {} errors and {} failures in {} with score {}/100.'
            '\n******************************************************************\n'.format(
                self.testsRun, len(self.errors), len(self.failures), time_taken, self.points
            ),
            file=to
        )
        if self.prerequisites_output:
            print('Found issues with your module:\n', file=to)
            print(self.prerequisites_output, file=to)
        else:
            print(self.test_output, file=to)
