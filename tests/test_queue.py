from .base_testcase import BaseTestCase


class QueueTests(BaseTestCase):

    class QueueWithData:
        def __init__(self, data):
            self.data = data

    class QueueWithDequeueAndLen:
        def __init__(self, x):
            self.x = x

        def __len__(self):
            return len(self.x)

        def dequeue(self):
            return self.x.pop(0)

    class QueueWithDequeueAndIsEmpty:
        def __init__(self, x):
            self.x = x

        def is_empty(self):
            return len(self.x) == 0

        def dequeue(self):
            return self.x.pop(0)

    def test_equal_queue_pass(self):
        q1 = self.QueueWithData([1, 2, 3])
        q2 = self.QueueWithData([1, 2, 3])

        self.test_case.assertQueueEqual(q1, q2)

    def test_unequal_queues_raise_assertion_error(self):
        q1 = self.QueueWithData([1, 3])
        q2 = self.QueueWithData([1, 2, 3])

        self.assertRaises(
            AssertionError, self.test_case.assertQueueEqual, q1, q2
        )

    def test_different_queue_classes_pass(self):
        data = [1, 2, 3]
        q1 = self.QueueWithData(data)
        q2 = self.QueueWithDequeueAndLen(data)
        q3 = self.QueueWithDequeueAndIsEmpty(data)

        self.test_case.assertQueueEqual(q1, q2)
        self.test_case.assertQueueEqual(q1, q3)
        self.test_case.assertQueueEqual(q2, q3)

    def test_different_queue_classes_fail(self):
        q1 = self.QueueWithData([1, 'g', 3])
        q2 = self.QueueWithDequeueAndLen(['1', '2', '3'])
        q3 = self.QueueWithDequeueAndIsEmpty([])

        self.assertRaises(AssertionError, self.test_case.assertEqual, q1, q2)
        self.assertRaises(AssertionError, self.test_case.assertEqual, q1, q3)
        self.assertRaises(AssertionError, self.test_case.assertEqual, q2, q3)

    def test_internal_data_remains_intact_after_test(self):
        data = [1, 2, 3]
        q1 = self.QueueWithData(data)
        q2 = self.QueueWithDequeueAndLen(data)

        self.test_case.assertQueueEqual(q1, q2)

        self.assertEqual(data, q1.data)
        self.assertEqual(data, q2.x)
