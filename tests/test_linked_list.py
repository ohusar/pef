from .base_testcase import BaseTestCase
from ..lib.default_structure_attributes import *


class StackTests(BaseTestCase):

    class LL:
        def __init__(self, s):
            self.start = s

    class Node:
        def __init__(self, d, n=None):
            self.value = d
            self.next = n

    def setUp(self):
        super().setUp()

        self.n1 = self.Node(1)
        self.n2 = self.Node(2, self.n1)
        self.n3 = self.Node(3, self.n2)

        self.ll1 = self.LL(self.n1)
        self.ll2 = self.LL(self.n2)
        self.ll3 = self.LL(self.n3)

    def test_extract_data_works(self):
        data = self.test_case._extract_data_from_linked_list(
            self.n3,
            DEFAULT_LINKED_LIST_NODE_ATTRS
        )
        self.assertEqual([3, 2, 1], data)
        data = self.test_case._extract_data_from_linked_list(
            self.n2,
            DEFAULT_LINKED_LIST_NODE_ATTRS
        )
        self.assertEqual([2, 1], data)

    def test_assert_linked_list_works_for_equal(self):
        self.test_case.assertLinkedListEqual(self.ll1, self.ll1)
        self.test_case.assertLinkedListEqual(self.ll2, self.ll2)
        self.test_case.assertLinkedListEqual(self.ll3, self.ll3)

    def test_assert_linked_list_fails_for_not_equal(self):
        self.assertRaises(
            AssertionError,
            self.test_case.assertLinkedListEqual,
            self.ll1,
            self.ll2
        )
        self.assertRaises(
            AssertionError,
            self.test_case.assertLinkedListEqual,
            self.ll1,
            self.ll3
        )
        self.assertRaises(
            AssertionError,
            self.test_case.assertLinkedListEqual,
            self.ll3,
            self.ll2
        )
