from os import remove
from unittest import TestCase

from ..lib.import_analyser import ImportAnalyser


class ImportAnalyserTests(TestCase):
    file_name1 = '_test1.py'
    file_name2 = '_test2.py'

    @classmethod
    def setUpClass(cls):
        f1 = open(ImportAnalyserTests.file_name1, 'w')
        f2 = open(ImportAnalyserTests.file_name2, 'w')

        f1.write(
            """
import os
import unittest
from random import random
parents, babies = (1, 1)
while babies < 100:
    a = 'This generation has {0} babies'.format(babies)
    parents, babies = (babies, parents + babies)
            """
        )

        f2.write(
            """
import sys
try:
    total = sum(int(arg) for arg in sys.argv[1:])
    a= total
    for i in range(3):
        pass
except ValueError:
    a = 'Please supply integer arguments'
            """
        )

        f1.close()
        f2.close()

    @classmethod
    def tearDownClass(cls):
        remove(ImportAnalyserTests.file_name1)
        remove(ImportAnalyserTests.file_name2)

    def test_returns_correct_imports(self):
        expected_output1 = {'os', 'unittest'}
        expected_output2 = {'sys'}

        output1 = ImportAnalyser.get_names_in_scope(ImportAnalyserTests.file_name1)
        output2 = ImportAnalyser.get_names_in_scope(ImportAnalyserTests.file_name2)

        for item in expected_output1:
            self.assertIn(item, output1)

        for item in expected_output2:
            self.assertIn(item, output2)
