from ..lib.class_structure_checker import ClassStructureChecker
import unittest


class ClassStructureCheckerTests(unittest.TestCase):
    class A:
        class B:
            pass

    class AA:
        pass

    class Example1:
        def __init__(self):
            self.e = 'e'

        def example(self):
            pass

        def example1(self):
            return 'pass'

    class Example2:
        def __init__(self):
            self.example1 = ''

        def example(self):
            pass

    def test_is_missing_class_correct(self):
        result = ClassStructureChecker._is_missing_class(
            ClassStructureCheckerTests, {'A': {'B': {}}}
        )
        self.assertFalse(result)

    def test_is_missing_class_wrong(self):
        result = ClassStructureChecker._is_missing_class(
            ClassStructureCheckerTests, {'AA': {'BB': {}}}
        )
        self.assertTrue(result)

    def test_can_check_attributes_correct(self):
        result = ClassStructureChecker._is_missing_class(
            ClassStructureCheckerTests, {'Example1': ['example', 'example1']}
        )
        self.assertFalse(result)

    def test_can_check_attributes_wrong(self):
        result = ClassStructureChecker._is_missing_class(
            ClassStructureCheckerTests, {'Example1': ['example1', 'example', 'example2']}
        )
        self.assertTrue(result)

    def test_instance_attrs_are_not_enough(self):
        result = ClassStructureChecker._is_missing_class(
            ClassStructureCheckerTests, {'Example2': ['example', 'example1']}
        )
        self.assertTrue(result)
