from os import remove
from unittest import TestCase

from ..lib.keyword_analyser import KeywordAnalyser


class KeywordAnalyserTests(TestCase):
    file_name1 = '_tmp_file1.py'
    file_name2 = '_tmp_file2.py'

    @classmethod
    def setUpClass(cls):
        f1 = open(KeywordAnalyserTests.file_name1, 'w')
        f2 = open(KeywordAnalyserTests.file_name2, 'w')

        f1.write(
            """
            parents, babies = (1, 1)
            while babies < 100:
                print('This generation has {0} babies'.format(babies))
                parents, babies = (babies, parents + babies)
            """
        )

        f2.write(
            """
            # This program adds up integers in the command line
            import sys
            from random import randrange
            try:
                total = sum(int(arg) for arg in sys.argv[1:])
                print('sum =', total)
                for i in range(3):
                    pass
            except ValueError:
                print('Please supply integer arguments')
            """
        )

        f1.close()
        f2.close()

    @classmethod
    def tearDownClass(cls):
        remove(KeywordAnalyserTests.file_name1)
        remove(KeywordAnalyserTests.file_name2)

    def test_returns_correct_keywords(self):
        expected_output1 = {'while'}
        expected_output2 = {'import', 'try', 'for', 'in', 'pass', 'except', 'from'}

        output1 = KeywordAnalyser.get_keywords_used(KeywordAnalyserTests.file_name1)
        output2 = KeywordAnalyser.get_keywords_used(KeywordAnalyserTests.file_name2)

        self.assertEqual(expected_output1, output1)
        self.assertEqual(expected_output2, output2)

    def test_returns_empty_for_empty_file(self):
        with open('empty_file', 'w') as _:
            self.assertEqual(set(), KeywordAnalyser.get_keywords_used('empty_file'))
            remove('empty_file')

    def test_raises_exception_for_non_existent_file(self):
        self.assertRaises(
            FileNotFoundError, KeywordAnalyser.get_keywords_used, 'nonexistent'
        )
