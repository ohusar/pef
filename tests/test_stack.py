from ..lib.custom_exceptions import AttributeNotFound
from .base_testcase import BaseTestCase


class StackTests(BaseTestCase):

    class Stack:

        def __init__(self, data):
            self.data = data

    class StackWithPop:

        def __init__(self, x):
            self.x = x

        def pop(self):
            if not self.is_empty():
                return self.x.pop()

        def is_empty(self):
            return len(self.x) == 0

    class StackWithPopBack:

        def __init__(self, x):
            self.x = x

        def __len__(self):
            return len(self.x)

        def pop_back(self):
            if len(self) > 0:
                return self.x.pop()

    class StackWithoutEmpty:

        def __init__(self, x):
            self.x = x

        def pop(self):
            return self.x.pop() if self.x else None

    def setUp(self):
        super().setUp()

        data = [1, 2, 5, 3]
        self.s1 = self.Stack(data)
        self.s2 = self.StackWithPop(data)
        self.s3 = self.StackWithPopBack(data)

        self.differentStack = self.Stack([1, 3, 4])
        self.emptyStack = self.StackWithPopBack([])
        self.incompleteStack = self.StackWithoutEmpty([1, 2, 3])

    def test_different_equal_stacks_dont_raise_error(self):
        self.test_case.assertStackEqual(self.s1, self.s2)
        self.test_case.assertStackEqual(self.s1, self.s3)
        self.test_case.assertStackEqual(self.s2, self.s3)

    def test_different_non_equal_stack_raise_error(self):
        self.assertRaises(
            AssertionError, self.test_case.assertStackEqual, self.s1, self.differentStack
        )
        self.assertRaises(
            AssertionError, self.test_case.assertStackEqual, self.s1, self.differentStack
        )
        self.assertRaises(
            AssertionError, self.test_case.assertStackEqual, self.s1, self.differentStack
        )
        self.assertRaises(
            AssertionError, self.test_case.assertStackEqual, self.emptyStack, self.differentStack
        )
        self.assertRaises(
            AssertionError, self.test_case.assertStackEqual, self.s1, self.emptyStack
        )
        self.assertRaises(
            AssertionError, self.test_case.assertStackEqual, self.s2, self.emptyStack
        )

    def test_stack_without_empty_raises_attribute_not_found(self):
        self.assertRaises(
            AttributeNotFound, self.test_case.assertStackEqual, self.s1, self.incompleteStack
        )
