from unittest import TestCase

from ..lib.attribute_finder import AttributeFinder
from ..lib.custom_exceptions import AttributeNotFound


class AttributeFinderTests(TestCase):

    class Example:
        def __init__(self, a='a', b='b', c='c'):
            self.a = a
            self._b = b
            self.cc = c

    def setUp(self):
        self.example = self.Example()
        self.correct_attributes = {
            'a': 'a',
            'b': {'b', '_b', 'bb'},
            'c': {'c', 'cc'}
        }
        self.failing_attributes = {
            'a': {'a', '_a'},
            'b': {'I DONT HAVE B ;('},
            'c': {'c', 'cc'},
        }

    def test_correct_attributes_are_parsed(self):
        getter_dictionary = AttributeFinder.get_instance_attributes_as_getters(
            self.example,
            self.correct_attributes,
        )
        self.assertEqual(self.example.a, getter_dictionary['a']())
        self.assertEqual(self.example._b, getter_dictionary['b']())
        self.assertEqual(self.example.cc, getter_dictionary['c']())

    def test_failing_attributes_raise_attribute_not_found(self):
        self.assertRaises(
            AttributeNotFound,
            AttributeFinder.get_instance_attributes_as_getters,
            self.example,
            self.failing_attributes,
        )

    def test_merge_attributes(self):
        merged_attributes = AttributeFinder.merge_attributes_into_one_function(
            self.example,
            self.correct_attributes,
        )
        merged_attributes = set(merged_attributes())
        expected_result = {self.example.a, self.example._b, self.example.cc}

        self.assertEqual(expected_result, merged_attributes)

    def test_merge_attributes_with_ordered_dict(self):
        merged_attributes = AttributeFinder.merge_attributes_into_one_function(
            self.example,
            self.correct_attributes,
            ['c', 'a', 'b']
        )
        expected_result = [self.example.cc, self.example.a, self.example._b]

        self.assertEqual(expected_result, merged_attributes())

    def test_get_is_empty_method(self):
        class Empty:
            def empty(self):
                return True

        class IsEmpty:
            def is_empty(self):
                return False

        class Len:
            def __len__(self):
                return 3

        class LenEmpty:
            def __len__(self):
                return 0

        class Nothing:
            def __init__(self):
                pass

        empty = Empty()
        is_empty = IsEmpty()
        with_len = Len()
        with_len_empty = LenEmpty()
        nothing = Nothing()

        is_empty_method1 = AttributeFinder.get_is_empty_method(empty)
        is_empty_method2 = AttributeFinder.get_is_empty_method(is_empty)
        is_empty_method3 = AttributeFinder.get_is_empty_method(with_len)
        is_empty_method4 = AttributeFinder.get_is_empty_method(with_len_empty)
        self.assertRaises(
            AttributeNotFound,
            AttributeFinder.get_is_empty_method,
            nothing
        )
        self.assertEqual(
            [True, False, False, True],
            [is_empty_method1(), is_empty_method2(), is_empty_method3(), is_empty_method4()]
        )
