from ..lib.image_comparator import ImageComparator
from ..lib.testable_canvas import Canvas
from os.path import isfile
from os import remove

from .base_testcase import BaseTestCase


class GraphicsTest(BaseTestCase):

    def setUp(self):
        super().setUp()

        self.files_to_be_deleted = set()

    def tearDown(self):
        for file in self.files_to_be_deleted:
            if isfile(file):
                remove(file)

    def test_empty_canvas_export_doesnt_fail(self):
        canvas = Canvas()

        self.files_to_be_deleted.add('test.png')
        canvas.extract_image('test.png')

        self.assertTrue(isfile('test.png'))

    def test_image_compare_works_for_same_images(self):
        canvas = Canvas(width=200, height=300)
        canvas.create_oval(10, 10, 150, 150, fill='red', outline='yellow')

        self.files_to_be_deleted.add('test1.png')
        canvas.extract_image('test1.png')

        self.files_to_be_deleted.add('test2.png')
        canvas.extract_image('test2.png')

        msqe, ssim = ImageComparator.compute('test1.png', 'test2.png')
        self.assertEqual(0, msqe)
        self.assertEqual(1.0, ssim)
        self.test_case.assertImageEqual('test1.png', 'test2.png')

    def test_image_compare_fails_for_different_images(self):
        canvas = Canvas(width=100, height=100)
        canvas.create_oval(10, 10, 150, 150, fill='red', outline='black')

        self.files_to_be_deleted.add('test1.png')
        canvas.extract_image('test1.png')

        canvas.create_rectangle(1, 1, 3, 3)
        self.files_to_be_deleted.add('test2.png')
        canvas.extract_image('test2.png')

        self.assertRaises(
            AssertionError, self.test_case.assertImageEqual, 'test1.png', 'test2.png'
        )

    def test_assert_image_equal_throws_file_not_found_for_non_existent_files(self):
        self.assertRaises(
            FileNotFoundError, self.test_case.assertImageEqual, 'non.png', 'existent.png'
        )
