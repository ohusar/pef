from unittest import TestCase
from ..pef import TestCase as Pef


class BaseTestCase(TestCase):

    def setUp(self):
        test_case = Pef

        def configure_mock(instance):
            instance.testing = True
        test_case.configure = configure_mock
        self.test_case = test_case()
