from ..lib.custom_exceptions import AttributeNotFound
from ..lib.default_structure_attributes import DEFAULT_NODE_ATTRS
from .base_testcase import BaseTestCase


class TreeTests(BaseTestCase):
    
    class Node:
        def __init__(self, value, children=None):
            if children is None:
                children = []

            self.value = value
            self.children = children
    
        def __repr__(self):
            return '{} ({})'.format(self.value, self.children)
    
    def setUp(self):
        super().setUp()
        
        self.node1 = self.Node(
            'A',
            children=[
                self.Node('B', [self.Node('E')]),
                self.Node('C', [self.Node('F'), self.Node('G')]),
                self.Node('D'),
            ]
        )
        
        self.node2 = self.Node(
            'A',
            children=[
                self.Node('B', [self.Node('E')]),
                self.Node('C', [self.Node('F'), self.Node('G')]),
                self.Node('D1'),
            ]
        )
        
        self.node3 = self.Node(
            'A',
            children=[
                self.Node('B', [self.Node('E')]),
                self.Node('C', [self.Node('F'), self.Node('G')]),
            ]
        )

    def test_equal_nodes(self):
        result1 = self.test_case._check_node_equal(self.node1, self.node1, DEFAULT_NODE_ATTRS)
        result2 = self.test_case._check_node_equal(self.node2, self.node2, DEFAULT_NODE_ATTRS)
        result3 = self.test_case._check_node_equal(self.node3, self.node3, DEFAULT_NODE_ATTRS)

        self.assertTrue(result1)
        self.assertTrue(result2)
        self.assertTrue(result3)

    def test_not_equal_nodes(self):
        result1 = self.test_case._check_node_equal(self.node1, self.node2, DEFAULT_NODE_ATTRS)
        result2 = self.test_case._check_node_equal(self.node1, self.node3, DEFAULT_NODE_ATTRS)
        result3 = self.test_case._check_node_equal(self.node2, self.node3, DEFAULT_NODE_ATTRS)

        self.assertFalse(result1)
        self.assertFalse(result2)
        self.assertFalse(result3)

    def test_empty_nodes_equal(self):
        node = self.Node(value='a')
        self.assertTrue(
            self.test_case._check_node_equal(node, node, DEFAULT_NODE_ATTRS)
        )

    def test_structure_without_correct_attrs_raise(self):
        class Wrong:
            def __init__(self):
                pass

        w = Wrong()

        self.assertRaises(
            AttributeNotFound,
            self.test_case._check_node_equal,
            self.node1,
            w,
            DEFAULT_NODE_ATTRS
        )

    def test_node_with_left_right_are_compatible(self):
        class LRNode:
            def __init__(self, value, l=None, r=None):
                self.value = value
                self.left = l
                self.right = r

        lr_node = LRNode('1', LRNode(2), LRNode(3))
        node = self.Node('1', children=[self.Node(2), self.Node(3)])
        self.test_case._check_node_equal(node, lr_node, DEFAULT_NODE_ATTRS)
