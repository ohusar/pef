from unittest import TestCase
from ..lib.default_structure_attributes import format_attribute_names, format_default_attributes,\
    format_all_defaults


class StructureAttributesTests(TestCase):

    def setUp(self):
        self.before_value = {'value', 'data', 'name'}
        self.after_value = {'value', 'data', 'name', '_value', '_data', '_name'}

        self.before_root = 'root'
        self.after_root = {'root', '_root'}

        self.before_tree_defaults_example = {
            'root': self.before_root,
            'value': self.before_value,
        }
        self.after_tree_defaults_example = {
            'root': self.after_root,
            'value': self.after_value,
        }

        self.before_stack_defaults_example = {'data': ('data',)}
        self.after_stack_defaults_example = {'data': {'_data', 'data'}}

        self.before_all_defaults_example = (
            self.before_tree_defaults_example,
            self.before_stack_defaults_example,
        )
        self.after_all_defaults_example = (
            self.after_tree_defaults_example,
            self.after_stack_defaults_example,
        )

    def test_formatting_attribute_names(self):
        actual_result = format_attribute_names(self.before_value)
        self.assertEqual(self.after_value, actual_result)

    def test_attribute_names_can_be_not_set(self):
        actual_result_tuple = ('test',)
        actual_result_list = ['test']
        actual_result_dict = {'test': 'test'}
        actual_result_tuple = format_attribute_names(actual_result_tuple)
        actual_result_list = format_attribute_names(actual_result_list)
        actual_result_dict = format_attribute_names(actual_result_dict)

        expected_result = {'test', '_test'}

        self.assertEqual(expected_result, actual_result_tuple)
        self.assertEqual(expected_result, actual_result_list)
        self.assertEqual(expected_result, actual_result_dict)

    def test_format_default_attributes(self):
        format_default_attributes(self.before_tree_defaults_example)
        self.assertEqual(self.after_tree_defaults_example, self.before_tree_defaults_example)

    def test_format_all_results(self):
        format_all_defaults(self.before_all_defaults_example)
        self.assertEqual(self.after_all_defaults_example, self.before_all_defaults_example)
